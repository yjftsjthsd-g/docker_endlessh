# docker_endlessh

A little container to run endlessh (https://github.com/skeeto/endlessh).

## Use

Simplest: grab docker-compose.yml and then run:
```
docker-compose up
```
Runs on port 2222 by default.

You can test with:
```
ssh -o Port=2222 127.0.0.1
```
If it's working, this will just hang.

