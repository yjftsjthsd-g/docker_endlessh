FROM debian:11-slim as build

RUN apt-get update && apt-get install -yq build-essential git

RUN git clone --depth 1 https://github.com/skeeto/endlessh.git /opt/endlessh
WORKDIR /opt/endlessh
RUN make

FROM gcr.io/distroless/base-debian11
LABEL maintainer "Brian Cole <docker@brianecole.com>"
COPY --from=build /opt/endlessh/endlessh /endlessh
EXPOSE 2222
USER nobody
ENTRYPOINT ["/endlessh"]
